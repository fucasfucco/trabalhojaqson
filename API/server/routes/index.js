const { Router } = require("express");
const routes = Router();
var cors = require('cors');
routes.use(cors({ origin: '*' }));
const ProdutoRout = require("./ProdutoRout");
routes.use("/api", ProdutoRout)
module.exports = routes;
