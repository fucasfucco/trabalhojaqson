const Produto = require('../model/ProdutoSchema');

module.exports = {
    listar: async (req, res) => {
        Produto.find((err, objetos) => {
            (err ? res.status(400).send(err) : res.status(200).json(objetos))
        })
    },

    incluir: async (req, res) => {
        let obj = new Produto(req.body)
        obj.creationDate = Date.now()
        obj.isActive = true
        console.log(obj)
        obj.save((err, obj) => {
            (err ? res.status(400).send(err) : res.status(200).json(obj))
        });
    },

    alterar: async (req, res) => {
        let obj = new Produto(req.body);
        Produto.updateOne({ _id: obj._id }, obj, function (err) {
            (err ? res.status(400).send(err) : res.status(200).json(obj));
        });
    },

    inativarProduto: async (req, res) => {
        Produto.findOne({ _id: req.params.id }, function (err, obj) {
            if (err)
                res.status(400).send(err);
            obj.isActive = false
            Produto.updateOne({ _id: req.params.id }, obj, function (err) {
                (err ? res.status(400).send(err) : res.status(200).json(obj));
            });
        });
    },

    excluir: async (req, res) => {
        Produto.deleteOne({ _id: req.params.id }, function (err) {
            (err ? res.status(400).send(err) : res.status(200).json("message:ok"));
        });
    },
    obterPeloId: async (req, res) => {
        Produto.findOne({ _id: req.params.id }, function (err, obj) {
            if (err)
                res.status(400).send(err);
            res.status(200).json(obj);
        });

    },

}