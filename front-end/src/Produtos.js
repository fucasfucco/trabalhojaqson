import "bootstrap/dist/css/bootstrap.min.css";
import ProdutosList from './ProdutosList';
import ProdutoForm from "./ProdutoForm";
import React, { useState, useEffect, useRef } from 'react'
import ProdutoSrv from "./services/ProdutoSrv";
import { Toast } from 'primereact/toast';

//import UsuariosForm from './UsuariosForm';

function Produtos() {
    const toastRef = useRef();

    const [produtos, setProdutos] = useState([])
    useEffect(() => {
        onClickAtualizar(); // ao inicializar execula método para atualizar
    }, []);

    const initialState = { _id: null, title: '', description: '', brand: '', nome: '', category: '', originType: '', creationDate: '', isActive: '' }
    const [produto, setProduto] = useState(initialState)
    const [editando, setEditando] = useState(false)

    const onClickAtualizar = () => {
        ProdutoSrv.listar().then(response => {
            setProdutos(response.data);
            toastRef.current.show({
                severity: 'success',
                summary: "Produtos atualizados",
                life: 3000
            });
        })
            .catch(e => {
                toastRef.current.show({
                    severity: 'error',
                    summary: e.message,
                    life: 3000
                });
            });
    }

    const salvar = () => {
        if (produto._id == null) { // inclussão
            ProdutoSrv.incluir(produto).then(response => {
                setEditando(false);
                onClickAtualizar();
                toastRef.current.show({ severity: 'success', summary: "Salvou", life: 2000 });
            })
                .catch(e => {
                    toastRef.current.show({ severity: 'error', summary: e.message, life: 4000 });
                });
        } else { // alteração
            ProdutoSrv.alterar(produto).then(response => {
                setEditando(false);
                onClickAtualizar();
                toastRef.current.show({ severity: 'success', summary: "Salvou", life: 2000 });
            })
                .catch(e => {
                    toastRef.current.show({ severity: 'error', summary: e.message, life: 4000 });
                });
        }
    }

    const onClickDelete = (_id) => {
        ProdutoSrv.excluir(_id).then(response => {
            onClickAtualizar();
            toastRef.current.show({
                severity: 'success',
                summary: "Excluído", life: 2000
            });
        })
            .catch(e => {
                toastRef.current.show({
                    severity: 'error',
                    summary: e.message, life: 4000
                });
            });
    }


    const onClickEditar = () => {
        setEditando(true);
    }

    const onClickInserir = () => {
        setProduto(initialState);
        setEditando(true);
    }

    const onClickSearch = (_id) => {
        if (_id != null) {
            ProdutoSrv.obterPeloId(_id).then(response => {
                setProdutos(response.data);
                console.log(produtos);
                toastRef.current.show({
                    severity: 'success',
                    summary: "Produto encontrado com sucesso",
                    life: 3000
                });
            })
                .catch(e => {
                    toastRef.current.show({
                        severity: 'error',
                        summary: e.message,
                        life: 3000
                    });
                })
        }
    }

    const onClickSalvar = () => {
        setEditando(false);
    }
    const onClickCancelar = () => {
        setEditando(false);
    }


    if (!editando) {
        return (
            <div>

                <ProdutosList produtos={produtos}
                    onClickAtualizar={onClickAtualizar}
                    onClickInserir={onClickInserir}
                    onClickEditar={onClickEditar}
                    setProduto={setProduto}
                    produto={produto}
                    onClickSearch={onClickSearch}
                    onClickDelete={onClickDelete}
                />
                <Toast ref={toastRef} />
            </div >
        );
    } else {
        return (
            <div>
                <ProdutoForm produto={produto}
                    setProduto={setProduto}
                    onClickSalvar={salvar}
                    onClickCancelar={onClickCancelar}
                />
                <Toast ref={toastRef} />
            </div >
        );
    }



}

export default Produtos;








