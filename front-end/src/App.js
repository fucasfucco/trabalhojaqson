import Produtos from './Produtos';
import PrimeReact from 'primereact/api';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css'


function App() {



  return (

    <div className="App">
      <br />
      <Produtos />
    </div>
  );
}

export default App;
