import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";



function ProdutosList(props) {

    const handleInputChange = (event) => {
        const { name, value } = event.target
        props.setProduto({ ...props.produto, [name]: value })
    }

    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Catálogo de Produtos</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <button onClick={props.onClickAtualizar} type="button"
                                className="btn btn-primary btn-sm">Atualizar Lista</button>
                        </li>
                        <li class="nav-item">
                            <button onClick={props.onClickInserir} type="button"
                                class="btn btn-secondary btn-lg btn-sm">Inserir</button>
                        </li>
                        <li class="nav-item">
                            <button type="button" onClick={props.onClickEditar}
                                className="btn btn-success btn-sm">Editar</button>
                        </li>
                        <li className='nav-item'>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" name="_id" type="text" value={props.produto._id} onChange={handleInputChange} placeholder="_id" aria-label="_id" />
                                <button class="btn btn-outline-success my-2 my-sm-0" type="button" onClick={() => props.onClickSearch(props.produto._id)}>Buscar</button>
                            </form>
                        </li>
                        <li className='nav-item'>
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" name="_id" type="text" value={props.produto._id} onChange={handleInputChange} placeholder="_id" aria-label="_id" />
                                <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onClick={() => props.onClickDelete(props.produto._id)}>Deletar</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Index</th>
                        <th>Id</th>
                        <th>title</th>
                        <th>description</th>
                        <th>brand</th>
                        <th>category</th>
                        <th>originType</th>
                        <th>creationDate</th>
                        <th>isActive</th>
                    </tr>
                </thead>
                <tbody>
                    <h4>Listagem de Produtos</h4>
                    {props.produtos.length > 0 ? props.produtos.map((o, index) => (
                        <tr key={index}>
                            <td>{index}</td>
                            <td>{o._id}</td>
                            <td>{o.title}</td>
                            <td>{o.description}</td>
                            <td>{o.brand}</td>
                            <td>{o.category}</td>
                            <td>{o.originType}</td>
                            <td>{o.creationDate}</td>
                            <td>{`${o.isActive}`}</td>
                        </tr>
                    )) : (
                        <tr key={1}>
                            <td>1</td>
                            <td>{props.produtos._id}</td>
                            <td>{props.produtos.title}</td>
                            <td>{props.produtos.description}</td>
                            <td>{props.produtos.brand}</td>
                            <td>{props.produtos.category}</td>
                            <td>{props.produtos.originType}</td>
                            <td>{props.produtos.creationDate}</td>
                            <td>{`${props.produtos.isActive}`}</td>
                        </tr>
                    )}
                </tbody>
            </table>




        </div>
    );
}
export default ProdutosList;